/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monte_carlo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Panda
 */
public class GUI2 extends JPanel {

    private int dim1;
    private int dim2;
    int pixelsize;
    List<Cell> list;
    List<Color> colors;

    public GUI2(int dim1, int dim2, Cell[][] table) {

        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        pixelsize = 1;
        this.setSize(d.height / dim1 * dim1, d.height / dim1 * dim1);
        pixelsize = this.getHeight() / dim1;
        setVisible(true);
        this.list = new ArrayList<>(dim1 * dim2);
        this.colors = new ArrayList<>(50);
        this.getColors();
        for (int i = 0; i < dim1; i++) {
            for (int j = 0; j < dim2; j++) {
                this.list.add(table[i][j]);
            }
        }
        this.repaint();

    }

    public void paint(Graphics g) {
        int limit = list.size();
        Cell cell;
        for (int i = 0; i < limit; i++) {
            cell = list.get(i);
            g.setColor(CColor.colorpool[cell.pre_energy+5]);
            g.fillRect(cell.getX() * pixelsize, cell.getY() * pixelsize, pixelsize, pixelsize);
        }
    }

    public void getColors() {
        Random rand = new Random();
        for (int i = 0; i < 150; i++) {
            this.colors.add(Color.white);
            this.colors.add(Color.red);
            this.colors.add(Color.green);
            this.colors.add(Color.blue);
            this.colors.add(Color.black);
            this.colors.add(Color.yellow);
            this.colors.add(new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));
        }
    }
}
