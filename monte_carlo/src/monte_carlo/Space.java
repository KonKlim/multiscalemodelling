/*
 * To change this license header, chose License Headers in Project Properties.
 * To change this template file, chose Tols | Templates
 * and open the template in the editor.
 */
package monte_carlo;

import static java.lang.Math.exp;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.max;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Panda
 */
public class Space {

    int length;
    int width;
    private int graintypes;
    int drmt, ypes;
    public boolean randed;
    int neighborhoodType;
    public Cell[][] cells;
    private Cell[][] tempCells;

    public Space(int length, int width) {
        this.graintypes = 0;
        this.drmt = 0;
        this.cells = new Cell[length][width];
        this.tempCells = new Cell[length][width];
        this.randed = false;
        this.length = length;
        this.width = width;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                this.cells[i][j] = new Cell();
                cells[i][j].setX(i);
                cells[i][j].setY(j);
                cells[i][j].setType(0);
            }
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j].setNeighbors(getMooreNeighbors(i, j));
            }
        }

    }

    void randomizeCells(int numberOfTypes) {
        Random rand = new Random();
        for (Cell[] dim : cells) {
            for (Cell cell : dim) {
                if (cell.getType() > 4 || cell.getType() == 0) {
                    cell.setType(rand.nextInt(numberOfTypes) + graintypes + 5);
                }
            }

        }
        graintypes += numberOfTypes;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j].setNeighbors(getMooreNeighbors(i, j));
            }
        }
    }

    void randomizeCells2(int numberOfTypes) {
        Random rand = new Random();
        int i = 0;
        for (i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                if (cells[i][j].getType() != 2) {
                    cells[i][j].setType(0);
                }
            }
        }
        i = 0;
        while (i < numberOfTypes) {
            int rand1 = rand.nextInt(length);
            int rand2 = rand.nextInt(width);
            if (cells[rand1][rand2].getType() == 0) {
                cells[rand1][rand2].setType(graintypes + 5);
                i = i + 1;
                graintypes++;
            }
        }
        for (i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j].setNeighbors(getMooreNeighbors(i, j));
            }
        }

    }

    void addInclusions(int numberOfInclusions, int diagonal, boolean circle
    ) {
        int h = 0;
        int i = 0;
        int j = 0;
        int k = 0;
        if (this.randed == false) {

            Random rand = new Random();
            while (h < numberOfInclusions) {
                int rand1 = rand.nextInt(length);
                int rand2 = rand.nextInt(width);
                if (cells[rand1][rand2].getType() != 1) {
                    cells[rand1][rand2].setType(1);
                }
                j = 0;
                k = 0;
                if (circle == true) {
                    for (i = 0; i < length; i++) {
                        for (j = 0; j < width; j++) {
                            if ((Math.sqrt((rand2 - j) * (rand2 - j) + (rand1 - i) * (rand1 - i))) < diagonal) {
                                cells[i][j].setType(1);
                            }
                        }

                    }
                    int a = 5;

                } else {
                    while (k < diagonal) {
                        j = 0;
                        while (j < diagonal) {
                            if (rand1 + k < length && rand2 + j < width) {
                                cells[rand1 + k][rand2 + j].setType(1);
                            }
                            j = j + 1;
                        }
                        k = k + 1;
                    }

                }
                h = h + 1;
            }

        } //System.out.println(rand1);
        // System.out.println(rand2);
        else {

            Random rand = new Random();
            List<Integer> bordersI = new ArrayList<>();
            List<Integer> bordersJ = new ArrayList<>();
            for (i = 0; i < length; i++) {
                for (j = 0; j < width; j++) {
                    if (i < length - 1) {
                        if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                            bordersI.add(i);
                            bordersJ.add(j);
                        }
                    }
                    if (j < width - 1) {
                        if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                            bordersI.add(i);
                            bordersJ.add(j);
                        }
                    }
                    if (i < length - 1 && j < width - 1) {
                        if (cells[i][j].getType() != cells[i + 1][j + 1].getType()) {
                            bordersI.add(i);
                            bordersJ.add(j);
                        }
                    }

                }
            }
            i = 0;
            while (h < numberOfInclusions) {
                int rannd = rand.nextInt(bordersI.size());
                cells[bordersI.get(rannd)][bordersJ.get(rannd)].setType(1);
                int rand1 = bordersI.get(rannd);
                int rand2 = bordersJ.get(rannd);
                j = 0;
                k = 0;
                if (circle == true) {
                    for (i = 0; i < length; i++) {
                        for (j = 0; j < width; j++) {
                            if ((Math.sqrt((rand2 - j) * (rand2 - j) + (rand1 - i) * (rand1 - i))) < diagonal) {
                                cells[i][j].setType(1);
                            }
                        }

                    }

                } else {
                    while (k < diagonal) {
                        j = 0;
                        while (j < diagonal) {
                            if (bordersI.get(rannd) + k < length && bordersJ.get(rannd) + j < width) {
                                cells[bordersI.get(rannd) + k][bordersJ.get(rannd) + j].setType(1);
                            }
                            j = j + 1;
                        }
                        k = k + 1;
                    }
                    bordersI.remove(rannd);
                    bordersJ.remove(rannd);

                    //System.out.println(rand1);
                    // System.out.println(rand2);
                }
                h = h + 1;
            }
        }
    }

    double colorBoundaries(int normal, int boundary) {
        Random rand = new Random();
        List<Integer> bordersI = new ArrayList<>();
        List<Integer> bordersJ = new ArrayList<>();
        int i, j;
        for (i = 0; i < length; i++) {
            for (j = 0; j < width; j++) {
                if (i < length - 1 && j < width - 1) {
                    if (cells[i][j].getType() != cells[i + 1][j + 1].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);
                    } else if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);
                    } else if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);

                    }
                } else if (j < width - 1) {
                    if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);
                    }
                } else if (i < length - 1) {
                    if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);
                    }
                }

            }
        }
        for (i = 0; i < length; i++) {
            for (j = 0; j < width; j++) {
                cells[i][j].pre_energy = normal;
            }

        }
        for (i = 0; i < bordersI.size(); i++) {
            cells[bordersI.get(i)][bordersJ.get(i)].pre_energy = boundary;
        }
        double gb = (double) bordersI.size() / (double) (length * width);
        return gb;
    }

    void growth() {
        this.randed = true;
        List<Integer> counters = new ArrayList<>();
        for (int i = 0; i < 900; i++) {
            counters.add(0);
        }

        int max;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                tempCells[i][j] = new Cell(cells[i][j]);
            }
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {

                if (cells[i][j].getType() == 0) {
                    java.util.Collections.fill(counters, new Integer(0));
                    if (i > 0) {
                        counters.set(tempCells[i - 1][j].getType(), counters.get(tempCells[i - 1][j].getType()) + 1);
                    }
                    if (i < length - 1) {
                        counters.set(tempCells[i + 1][j].getType(), counters.get(tempCells[i + 1][j].getType()) + 1);
                    }
                    if (j > 0) {
                        counters.set(tempCells[i][j - 1].getType(), counters.get(tempCells[i][j - 1].getType()) + 1);
                    }
                    if (j < width - 1) {
                        counters.set(tempCells[i][j + 1].getType(), counters.get(tempCells[i][j + 1].getType()) + 1);
                    }
                    counters.set(0, 0);
                    counters.set(1, 0);
                    counters.set(2, 0);
                    max = Collections.max(counters);
                    int index = counters.indexOf(max);
                    cells[i][j].setType(index);

                }
            }
        }
    }

    void monteCarlo() {

        int energy;
        Random rand = new Random();
        int hit;
        int k, randd, randI, randJ, i, j;
        k = 0;
        List<Integer> randsI = new ArrayList<>();
        List<Integer> randsJ = new ArrayList<>();
        for (i = 0; i < length; i++) {
            for (j = 0; j < width; j++) {
                if (cells[i][j].getType() > 4) {
                    randsI.add(i);
                    randsJ.add(j);
                }
            }

        }

        while (randsI.size() > 0) {

            energy = 0;
            randd = rand.nextInt(randsI.size());
            randI = randsI.get(randd);
            randJ = randsJ.get(randd);
            randsI.remove(randd);
            randsJ.remove(randd);

            for (Cell neighbor : cells[randI][randJ].getNeighbors()) {
                if (neighbor.getType() == cells[randI][randJ].getType()) {
                    energy++;
                } else {
                    energy--;
                }
            }
            if (energy <= 0) {
                hit = rand.nextInt(cells[randI][randJ].getNeighbors().size());
                cells[randI][randJ].setType(cells[randI][randJ].getNeighbors().get(hit).getType());
            }

        }
    }

    void drm(double J, int nuc_type, int nuc_const, int iteration, int grain_types) {
        if (nuc_type != 2) {
            this.nucleation(nuc_type, nuc_const, iteration, grain_types);
        }
        Random rand = new Random();
        int i, j, randd, randI, randJ, hit, hit_type;
        double energy = 0;
        List<Integer> randsI = new ArrayList<>();
        List<Integer> randsJ = new ArrayList<>();
        for (i = 0; i < length; i++) {
            for (j = 0; j < width; j++) {
                if (cells[i][j].getType() > 4) {
                    randsI.add(i);
                    randsJ.add(j);
                    cells[i][j].setNeighbors(getMooreNeighbors(i, j));
                }
            }
        }

        while (randsI.size() > 0) {

            energy = 0;
            randd = rand.nextInt(randsI.size());
            randI = randsI.get(randd);
            randJ = randsJ.get(randd);
            randsI.remove(randd);
            randsJ.remove(randd);
            System.out.println(randI);
            System.out.println(randJ);
            List<Cell> neighs=cells[randI][randJ].getNeighbors();
            int sizee=neighs.size();
            if(sizee>0){
            hit = rand.nextInt(sizee);
            hit_type = neighs.get(hit).getType();
            if (hit_type > 770) {
                energy += cells[randI][randJ].pre_energy;
                for (Cell neighbor : cells[randI][randJ].getNeighbors()) {
                    if (neighbor.getType() == cells[randI][randJ].getType()) {
                        energy -= J;
                    } else if (neighbor.getType() == hit_type) {
                        energy += J;
                    }
                }
                if (energy >= 0) {
                    cells[randI][randJ].setType(hit_type);
                    cells[randI][randJ].pre_energy=0;
                }
            }

        }}

    }

    void nucleation(int nuc_type, int nuc_const, int iteration, int grain_types) {
        List<Integer> randsI = new ArrayList<>();
        List<Integer> randsJ = new ArrayList<>();
        Random rand = new Random();
        int i, j, randd, randI, randJ;
        if (nuc_type == 0) {
            for (i = 0; i < length; i++) {
                for (j = 0; j < width; j++) {
                    if (cells[i][j].getType() < 770) {
                        randsI.add(i);
                        randsJ.add(j);
                    }
                }

            }
        } else {
            for (i = 0; i < length; i++) {
                for (j = 0; j < width; j++) {
                    if (cells[i][j].getType() < 770) {
                        if (i < length - 1 && j < width - 1) {
                            if (cells[i][j].getType() != cells[i + 1][j + 1].getType()) {
                                if (cells[i][j].getType() < 770) {
                                    randsI.add(i);
                                    randsJ.add(j);
                                }
                            } else if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                                if (cells[i][j].getType() < 770) {
                                    randsI.add(i);
                                    randsJ.add(j);
                                }
                            } else if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                                if (cells[i][j].getType() < 770) {
                                    randsI.add(i);
                                    randsJ.add(j);
                                }

                            }
                        } else if (j < width - 1) {
                            if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                                if (cells[i][j].getType() < 770) {
                                    randsI.add(i);
                                    randsJ.add(j);
                                }
                            }
                        } else if (i < length - 1) {
                            if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                                if (cells[i][j].getType() < 770) {
                                    randsI.add(i);
                                    randsJ.add(j);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (nuc_const == 0) {
            i = 0;
            while (i < grain_types) {
                if (randsI.size() > 0) {
                    randd = rand.nextInt(randsI.size());
                    randI = randsI.get(randd);
                    randJ = randsJ.get(randd);
                    randsI.remove(randd);
                    randsJ.remove(randd);
                    cells[randI][randJ].setType(drmt + 770);
                    cells[randI][randJ].pre_energy=0;
                    drmt++;
                }
                i++;
            }

        } else if (nuc_const == 1) {
            i = 0;
            while (i < grain_types * iteration + 1) {
                if (randsI.size() > 0) {
                    randd = rand.nextInt(randsI.size());
                    randI = randsI.get(randd);
                    randJ = randsJ.get(randd);
                    randsI.remove(randd);
                    randsJ.remove(randd);
                    cells[randI][randJ].setType(drmt + 770);
                    cells[randI][randJ].pre_energy=0;
                    drmt++;
                }
                i++;
            }
        }

    }

    void nucleation2(int nuc_type, int grain_types) {
        List<Integer> randsI = new ArrayList<>();
        List<Integer> randsJ = new ArrayList<>();
        Random rand = new Random();
        int i, j, randd, randI, randJ;
        if (nuc_type == 0) {
            for (i = 0; i < length; i++) {
                for (j = 0; j < width; j++) {
                    if (cells[i][j].getType() < 770) {
                        randsI.add(i);
                        randsJ.add(j);
                    }
                }

            }
        } else {
            for (i = 0; i < length; i++) {
                for (j = 0; j < width; j++) {
                    if (cells[i][j].getType() < 770) {
                        if (i < length - 1 && j < width - 1) {
                            if (cells[i][j].getType() != cells[i + 1][j + 1].getType()) {
                                randsI.add(i);
                                randsJ.add(j);
                            } else if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                                randsI.add(i);
                                randsJ.add(j);
                            } else if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                                randsI.add(i);
                                randsJ.add(j);

                            }
                        } else if (j < width - 1) {
                            if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                                randsI.add(i);
                                randsJ.add(j);
                            }
                        } else if (i < length - 1) {
                            if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                                randsI.add(i);
                                randsJ.add(j);
                            }
                        }

                    }
                }
            }
        }
        i = 0;
        while (i < grain_types) {
            if (randsI.size() > 0) {
                randd = rand.nextInt(randsI.size());
                randI = randsI.get(randd);
                randJ = randsJ.get(randd);
                randsI.remove(randd);
                randsJ.remove(randd);
                cells[randI][randJ].setType(drmt + 770);
                cells[randI][randJ].pre_energy=0;
                drmt++;
            }
            i++;

        }
    }

    void preserveGrains(int numberToPreserve
    ) {
        Random rand = new Random();
        int hit;
        List<Integer> availableTypes = new ArrayList<>();
        List<Integer> typesToPreserve = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                if (!availableTypes.contains(cells[i][j].getType()) && cells[i][j].getType() > 4) {
                    availableTypes.add(cells[i][j].getType());
                }
            }
        }
        int k = 0;
        while (k < numberToPreserve) {

            hit = rand.nextInt(availableTypes.size());
            typesToPreserve.add(availableTypes.get(hit));
            availableTypes.remove(hit);
            k++;
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                if (typesToPreserve.contains(cells[i][j].getType())) {
                    cells[i][j].setType(2);
                }
            }

        }
        k = k + 1;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j].setNeighbors(getMooreNeighbors(i, j));
            }
        }
    }


    /*
    void monteCarlo2(double kT) {

        int energy;
        double probability;
        Random rand = new Random();
        int hit;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                tempCells[i][j] = new Cell(cells[i][j]);
            }
        }

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                energy = 0;
                for (Cell neighbor : tempCells[i][j].getNeighbors()) {
                    if (neighbor.getType() == tempCells[i][j].getType()) {
                        energy++;
                    } else {
                        energy--;
                    }
                }
                if (energy <= 0) {
                    hit = rand.nextInt(tempCells[i][j].getNeighbors().size());
                    cells[i][j].setType(tempCells[i][j].getNeighbors().get(hit).getType());
                } else {
                    probability = exp(-energy / (kT));
                    if (probability > rand.nextDouble()) {
                        hit = rand.nextInt(tempCells[i][j].getNeighbors().size());
                        cells[i][j].setType(tempCells[i][j].getNeighbors().get(hit).getType());
                    }

                }

            }
        }

    }

    void testFill() {
        int counter = 1;
        for (int i = 0; i < 180; i++) {
            for (int j = 0; j < 150; j++) {
                cells[i][j].setType(1);

            }
        }
        for (int i = 0; i < 170; i++) {
            for (int j = 150; j < 300; j++) {
                cells[i][j].setType(2);

            }
        }
        for (int i = 180; i < 300; i++) {
            for (int j = 0; j < 150; j++) {
                cells[i][j].setType(3);

            }
        }
        for (int i = 170; i < 300; i++) {
            for (int j = 150; j < 300; j++) {
                cells[i][j].setType(4);

            }
        }
    }
     */
    List<Cell> getMooreNeighbors(int x, int y
    ) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0 && cells[x - 1][y].getType() > 4) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1 && cells[x + 1][y].getType() > 4) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0 && cells[x][y - 1].getType() > 4) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1 && cells[x][y + 1].getType() > 4) {
            list.add(cells[x][y + 1]);
        }
        if (x > 0 && y > 0 && cells[x - 1][y - 1].getType() > 4) {
            list.add(cells[x - 1][y - 1]);
        }
        if (x > 0 && y < width - 1 && cells[x - 1][y + 1].getType() > 4) {
            list.add(cells[x - 1][y + 1]);
        }
        if (x < length - 1 && y > 0 && cells[x + 1][y - 1].getType() > 4) {
            list.add(cells[x + 1][y - 1]);
        }
        if (x < length - 1 && y < width - 1 && cells[x + 1][y + 1].getType() > 4) {
            list.add(cells[x + 1][y + 1]);
        }

        return list;

    }

    List<Cell> getVonNeumannNeighbors(int x, int y
    ) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }

        return list;

    }

    /*
    List<Cell> getHexagonalLeftNeighbors(int x, int y) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }
        if (x > 0 && y < width - 1) {
            list.add(cells[x - 1][y + 1]);
        }
        if (x < length - 1 && y > 0) {
            list.add(cells[x + 1][y - 1]);
        }

        return list;

    }

    List<Cell> getHexagonalRightNeighbors(int x, int y) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }
        if (x > 0 && y > 0) {
            list.add(cells[x - 1][y - 1]);
        }
        if (x < length - 1 && y < width - 1) {
            list.add(cells[x + 1][y + 1]);
        }

        return list;

    }

    List<Cell> getPentagonalNeighbors(int x, int y) {
        List<Cell> list = getMooreNeighbors(x, y);
        Random rand = new Random();
        switch (rand.nextInt(4)) {
            case 0:
                list.remove(cells[x - 1][y - 1]);
                list.remove(cells[x - 1][y]);
                list.remove(cells[x - 1][y + 1]);
                break;
            case 1:
                list.remove(cells[x - 1][y - 1]);
                list.remove(cells[x][y - 1]);
                list.remove(cells[x + 1][y - 1]);
                break;
            case 2:
                list.remove(cells[x + 1][y - 1]);
                list.remove(cells[x + 1][y]);
                list.remove(cells[x + 1][y + 1]);
                break;
            case 3:
                list.remove(cells[x + 1][y + 1]);
                list.remove(cells[x][y + 1]);
                list.remove(cells[x + 1][y + 1]);
                break;
        }
        return list;

    }
     */
    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

}
