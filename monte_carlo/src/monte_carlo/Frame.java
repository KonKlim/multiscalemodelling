package monte_carlo;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */


/**
 *
 * @author Panda
 */
public class Frame extends JFrame {

    public Frame(int dim1, int dim2, Cell[][] table) {
        super("Grain Growth");
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
      this.setSize(d.height / dim1 * dim1, d.height / dim1 * dim1);
 
        JPanel gui = new GUI(dim1,dim2,table);

        add(gui);

      //  pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
