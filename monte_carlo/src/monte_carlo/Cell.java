/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monte_carlo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Panda
 */
public class Cell {

    private int type;
    private List<Cell> neighbors;
    private int x;
    private int y;
    int pre_energy;

    public Cell() {
        this.neighbors=new ArrayList<>();
        this.pre_energy=0;
    }

    public Cell(Cell cell) {
        this.type = cell.type;
        this.neighbors = cell.neighbors;
        this.x = cell.x;
        this.y = cell.y;
        this.pre_energy=0;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Cell> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(List<Cell> neighbors) {
        this.neighbors = neighbors;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
