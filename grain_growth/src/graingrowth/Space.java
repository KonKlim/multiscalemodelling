/*
 * To change this license header, chose License Headers in Project Properties.
 * To change this template file, chose Tols | Templates
 * and open the template in the editor.
 */
package graingrowth;

import static java.lang.Math.exp;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.max;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Panda
 */
public class Space {

    private int length;
    private int width;
    private int graintypes;
    public boolean randed;
    int neighborhoodType;
    public Cell[][] cells;
    private Cell[][] tempCells;

    public Space(int length, int width) {
        this.graintypes = 0;
        this.cells = new Cell[length][width];
        this.tempCells = new Cell[length][width];
        this.randed = false;
        this.length = length;
        this.width = width;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                this.cells[i][j] = new Cell();
                cells[i][j].setX(i);
                cells[i][j].setY(j);
                cells[i][j].setType(0);
            }
        }

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j].setNeighbors(getVonNeumannNeighbors(i, j));
            }
        }

    }

    void randomizeCells(int numberOfTypes) {
        Random rand = new Random();
        int i = 0;
        while (i < numberOfTypes) {
            int rand1 = rand.nextInt(length);
            int rand2 = rand.nextInt(width);
            if (cells[rand1][rand2].getType() == 0) {
                cells[rand1][rand2].setType(graintypes + 2);
                i = i + 1;
                graintypes++;
            }
        }

    }

    void setCellsEvenly(int numberOfTypes) {

        int counter = 1;

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j].setType(counter);
                counter++;
                if (counter > numberOfTypes) {
                    counter = 1;
                }
            }
        }
    }

    void addInclusions(int numberOfInclusions, int diagonal, boolean circle) {
        int h=0;
        int i = 0;
        int j = 0;
        int k = 0;
        if (this.randed == false) {

            Random rand = new Random();
            while (h < numberOfInclusions) {
                int rand1 = rand.nextInt(length);
                int rand2 = rand.nextInt(width);
                if (cells[rand1][rand2].getType() != 1) {
                    cells[rand1][rand2].setType(1);}
                    j = 0;
                    k = 0;
                    if (circle == true) {
                        for (i = 0; i < length; i++) {
                            for (j = 0; j < width; j++) {
                                if ((Math.sqrt((rand2 - j) * (rand2 - j) + (rand1 - i) * (rand1 - i))) < diagonal) {
                                    cells[i][j].setType(1);
                                }
                            }
                            

                        }
                        int a=5;

                    } else {
                        while (k < diagonal) {
                            j = 0;
                            while (j < diagonal) {
                                if (rand1 + k < length && rand2 + j < width) {
                                    cells[rand1 + k][rand2 + j].setType(1);
                                }
                                j = j + 1;
                            }
                            k = k + 1;
                        }
                        
                    }
                    h = h + 1;
                }
            
        } //System.out.println(rand1);
        // System.out.println(rand2);
        else {

            Random rand = new Random();
            List<Integer> bordersI = new ArrayList<>();
            List<Integer> bordersJ = new ArrayList<>();
            for (i = 0; i < length; i++) {
                for (j = 0; j < width; j++) {
                    if (i < length - 1) {
                        if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                            bordersI.add(i);
                            bordersJ.add(j);
                        }
                    }
                    if (j < width - 1) {
                        if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                            bordersI.add(i);
                            bordersJ.add(j);
                        }
                    }
                    if (i < length - 1 && j < width - 1) {
                        if (cells[i][j].getType() != cells[i + 1][j + 1].getType()) {
                            bordersI.add(i);
                            bordersJ.add(j);
                        }
                    }

                }
            }
            i = 0;
            while (h < numberOfInclusions) {
                int rannd = rand.nextInt(bordersI.size());
                cells[bordersI.get(rannd)][bordersJ.get(rannd)].setType(1);
                int rand1 = bordersI.get(rannd);
                int rand2 = bordersJ.get(rannd);
                j = 0;
                k = 0;
                if (circle == true) {
                    for (i = 0; i < length; i++) {
                        for (j = 0; j < width; j++) {
                            if ((Math.sqrt((rand2 - j) * (rand2 - j) + (rand1 - i) * (rand1 - i))) < diagonal) {
                                cells[i][j].setType(1);
                            }
                        }

                    }

                } else {
                    while (k < diagonal) {
                        j = 0;
                        while (j < diagonal) {
                            if (bordersI.get(rannd) + k < length && bordersJ.get(rannd) + j < width) {
                                cells[bordersI.get(rannd) + k][bordersJ.get(rannd) + j].setType(1);
                            }
                            j = j + 1;
                        }
                        k = k + 1;
                    }
                    bordersI.remove(rannd);
                    bordersJ.remove(rannd);
                    
                    //System.out.println(rand1);
                    // System.out.println(rand2);

                }
                h = h + 1;
            }
        }
    }

    double colorBoundaries() {
        Random rand = new Random();
        List<Integer> bordersI = new ArrayList<>();
        List<Integer> bordersJ = new ArrayList<>();
        int i, j;
        for (i = 0; i < length; i++) {
            for (j = 0; j < width; j++) {
                if (i < length - 1) {
                    if (cells[i][j].getType() != cells[i + 1][j].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);
                    }
                }
                if (j < width - 1) {
                    if (cells[i][j].getType() != cells[i][j + 1].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);
                    }
                }
                if (i < length - 1 && j < width - 1) {
                    if (cells[i][j].getType() != cells[i + 1][j + 1].getType()) {
                        bordersI.add(i);
                        bordersJ.add(j);
                    }
                }

            }
        }
        for (i = 0; i < bordersI.size(); i++) {
            cells[bordersI.get(i)][bordersJ.get(i)].setType(1);
        }
        for (i = 0; i < length; i++) {
            for (j = 0; j < width; j++) {
                if (cells[i][j].getType() == 1) {
                } else {
                    cells[i][j].setType(0);
                }
            }

        }
        double gb = (double) bordersI.size() / (double) (length * width);
        return gb;
    }

    void growth() {
        this.randed = true;
        List<Integer> counters = new ArrayList<>();
        for (int i = 0; i < 900; i++) {
            counters.add(0);
        }

        int max;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                tempCells[i][j] = new Cell(cells[i][j]);
            }
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {

                if (cells[i][j].getType() == 0) {
                    java.util.Collections.fill(counters, new Integer(0));
                    if (i > 0) {
                        counters.set(tempCells[i - 1][j].getType(), counters.get(tempCells[i - 1][j].getType()) + 1);
                    }
                    if (i < length - 1) {
                        counters.set(tempCells[i + 1][j].getType(), counters.get(tempCells[i + 1][j].getType()) + 1);
                    }
                    if (j > 0) {
                        counters.set(tempCells[i][j - 1].getType(), counters.get(tempCells[i][j - 1].getType()) + 1);
                    }
                    if (j < width - 1) {
                        counters.set(tempCells[i][j + 1].getType(), counters.get(tempCells[i][j + 1].getType()) + 1);
                    }
                    counters.set(0, 0);
                    counters.set(1, 0);
                    max = Collections.max(counters);
                    int index = counters.indexOf(max);
                    cells[i][j].setType(index);

                }
            }
        }
    }

    /*
    void monteCarlo() {

        int energy;
        Random rand = new Random();
        int hit;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                tempCells[i][j] = new Cell(cells[i][j]);
            }
        }

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                energy = 0;
                for (Cell neighbor : tempCells[i][j].getNeighbors()) {
                    if (neighbor.getType() == tempCells[i][j].getType()) {
                        energy++;
                    } else {
                        energy--;
                    }
                }
                if (energy <= 0) {
                    hit = rand.nextInt(tempCells[i][j].getNeighbors().size());
                    cells[i][j].setType(tempCells[i][j].getNeighbors().get(hit).getType());
                }

            }
        }

    }

    void monteCarlo2(double kT) {

        int energy;
        double probability;
        Random rand = new Random();
        int hit;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                tempCells[i][j] = new Cell(cells[i][j]);
            }
        }

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                energy = 0;
                for (Cell neighbor : tempCells[i][j].getNeighbors()) {
                    if (neighbor.getType() == tempCells[i][j].getType()) {
                        energy++;
                    } else {
                        energy--;
                    }
                }
                if (energy <= 0) {
                    hit = rand.nextInt(tempCells[i][j].getNeighbors().size());
                    cells[i][j].setType(tempCells[i][j].getNeighbors().get(hit).getType());
                } else {
                    probability = exp(-energy / (kT));
                    if (probability > rand.nextDouble()) {
                        hit = rand.nextInt(tempCells[i][j].getNeighbors().size());
                        cells[i][j].setType(tempCells[i][j].getNeighbors().get(hit).getType());
                    }

                }

            }
        }

    }

    void testFill() {
        int counter = 1;
        for (int i = 0; i < 180; i++) {
            for (int j = 0; j < 150; j++) {
                cells[i][j].setType(1);

            }
        }
        for (int i = 0; i < 170; i++) {
            for (int j = 150; j < 300; j++) {
                cells[i][j].setType(2);

            }
        }
        for (int i = 180; i < 300; i++) {
            for (int j = 0; j < 150; j++) {
                cells[i][j].setType(3);

            }
        }
        for (int i = 170; i < 300; i++) {
            for (int j = 150; j < 300; j++) {
                cells[i][j].setType(4);

            }
        }
    }
     */
 /*private List<Cell> getMooreNeighbors(int x, int y) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }
        if (x > 0 && y > 0) {
            list.add(cells[x - 1][y - 1]);
        }
        if (x > 0 && y < width - 1) {
            list.add(cells[x - 1][y + 1]);
        }
        if (x < length - 1 && y > 0) {
            list.add(cells[x + 1][y - 1]);
        }
        if (x < length - 1 && y < width - 1) {
            list.add(cells[x + 1][y + 1]);
        }

        return list;

    }*/
    List<Cell> getVonNeumannNeighbors(int x, int y
    ) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }

        return list;

    }

    /*
    List<Cell> getHexagonalLeftNeighbors(int x, int y) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }
        if (x > 0 && y < width - 1) {
            list.add(cells[x - 1][y + 1]);
        }
        if (x < length - 1 && y > 0) {
            list.add(cells[x + 1][y - 1]);
        }

        return list;

    }

    List<Cell> getHexagonalRightNeighbors(int x, int y) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }
        if (x > 0 && y > 0) {
            list.add(cells[x - 1][y - 1]);
        }
        if (x < length - 1 && y < width - 1) {
            list.add(cells[x + 1][y + 1]);
        }

        return list;

    }

    List<Cell> getPentagonalNeighbors(int x, int y) {
        List<Cell> list = getMooreNeighbors(x, y);
        Random rand = new Random();
        switch (rand.nextInt(4)) {
            case 0:
                list.remove(cells[x - 1][y - 1]);
                list.remove(cells[x - 1][y]);
                list.remove(cells[x - 1][y + 1]);
                break;
            case 1:
                list.remove(cells[x - 1][y - 1]);
                list.remove(cells[x][y - 1]);
                list.remove(cells[x + 1][y - 1]);
                break;
            case 2:
                list.remove(cells[x + 1][y - 1]);
                list.remove(cells[x + 1][y]);
                list.remove(cells[x + 1][y + 1]);
                break;
            case 3:
                list.remove(cells[x + 1][y + 1]);
                list.remove(cells[x][y + 1]);
                list.remove(cells[x + 1][y + 1]);
                break;
        }
        return list;

    }
     */
    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

}
