/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graingrowth;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Panda
 */
public class DataUtils {

    public static double[][] records;
    public static int width;
    public static int height;
    static String[] results;

    public static void saveTxt(Cell[][] array, String filename) throws FileNotFoundException {

        PrintWriter out = new PrintWriter(filename);
        out.println(array.length + " " + array[0].length);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                out.println(array[i][j].getX() + " " + array[i][j].getY() + " " + array[i][j].getType());
            }
        }
        out.close();
    }

    public static Cell[][] loadTxt(String path) throws FileNotFoundException, IOException {

        File fileDir = new File(path);

        BufferedReader bfr = null;
        try {
            bfr = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(fileDir), "Cp1250"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DataUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        String parts[] = null;
        try {
            parts = bfr.readLine().split(" ");
        } catch (IOException ex) {
        }
        width = Integer.parseInt(parts[0]);
        height = Integer.parseInt(parts[1]);
        Cell[][] newcells = new Cell[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                parts = bfr.readLine().split(" ");
                newcells[i][j] = new Cell();
                newcells[i][j].setX(Integer.parseInt(parts[0]));
                newcells[i][j].setY(Integer.parseInt(parts[1]));
                newcells[i][j].setType(Integer.parseInt(parts[2]));
            }
        }

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                newcells[i][j].setNeighbors(getVonNeumannNeighbors(i, j,  height, width, newcells));
            }
        }
        bfr.close();
        return newcells;
    }

    static List<Cell> getVonNeumannNeighbors(int x, int y, int length, int width, Cell[][]cells) {
        List<Cell> list = new ArrayList<>(3);
        if (x > 0) {
            list.add(cells[x - 1][y]);
        }
        if (x < length - 1) {
            list.add(cells[x + 1][y]);
        }
        if (y > 0) {
            list.add(cells[x][y - 1]);
        }
        if (y < width - 1) {
            list.add(cells[x][y + 1]);
        }

        return list;

    }
}
